package dataType;

import java.net.*;

public class Passenger extends User {
	private double slon, sla, dlon, dla;
	private double fee = 0;
	private String nickname = null;
	private Driver match = null;
	
	public Passenger(String name, String password) {
		super(name, password);
	}

	public Passenger(String name, String password, String phoneNumber, String nickname) {
		super(name, password, phoneNumber);
		this.nickname = nickname;
	}

	public Passenger(String id, String password, double slon, double sla,
			double dlon, double dla, Driver match, String nickname) {
		super(id, password);
		updateData(slon, sla, dlon, dla);
		updateMatch(match);
		this.nickname = nickname;
	}
	
	public String getNickName() {
		return this.nickname;
	}

	public boolean updateData(double slon, double sla, double dlon, double dla) {
		this.slon = slon;
		this.sla = sla;
		this.dlon = dlon;
		this.dla = dla;
		return true;
	}
	
	public boolean updateMatch(Driver match) {
		try {
			this.match = match;
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean updateFee(double fee) {
		this.fee = fee;
		return true;
	}
	
	public double getFee() {
		return this.fee;
	}
	
	public String getSlonToString() {
		return String.valueOf(this.slon);
	}
	
	public String getSlaToString() {
		return String.valueOf(this.sla);
	}
	
	public String getDlonToString() {
		return String.valueOf(this.dlon);
	}
	
	public String getDlaToString() {
		return String.valueOf(this.dla);
	}
	
	public boolean setMatch(Driver driver) {
		this.match = driver;
		return true;
	}
	
	public Driver getMatch() {
		return this.match;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Passenger) {
			return ((Passenger) o).name.equals(name);
		} else {
			return false;
		}
	}
}
