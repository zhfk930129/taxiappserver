package dataType;

import java.net.*;

public class Driver extends User {
	private double lon, la;
	private String carNumber = null;
	private Passenger match = null;

	public Driver(String name, String password) {
		super(name, password);
	}
	
	public Driver(String name, String password, String phoneNumber, String carNumber) {
		super(name, password, phoneNumber);
		this.carNumber = carNumber;
	}

	public Driver(String id, String password, double lon, double la, Passenger match) {
		super(id, password);
		updateData(lon, la, match);
	}
	
	public String getCarNumber() {
		return this.carNumber;
	}

	public boolean updateData(double lon, double la, Passenger match) {
		this.lon = lon;
		this.la = la;
		this.match = match;
		return true;
	}
	
	public boolean setMatch(Passenger p) {
		this.match = p;
		return true;
	}
	
	public String getLonToString() {
		return String.valueOf(this.lon);
	}
	
	public String getLaToString() {
		return String.valueOf(this.la);
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Driver) {
			return ((Driver) o).name.equals(name);
		} else {
			return false;
		}
	}
}
