package dataType;

public class User {
	protected String name;
	protected String password;
	protected String phoneNumber;
	protected String IPAddress;
	
	public User() {	
	}
	
	public User(String name, String password) {
		this.name = name;
		this.password = password;
	}
	
	public User(String name, String password, String phoneNumber) {
		this.name = name;
		this.password = password;
		this.phoneNumber = phoneNumber;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getPhoneNumber() {
		return this.phoneNumber;
	}
	
	public boolean setIPAddress(String IP) {
		this.IPAddress = IP;
		return true;
	}
	
	public String getIPAddress() {
		return this.IPAddress;
	}
	
	public boolean updatePassword(String s) {
		this.password = s;
		// TODO password check
		return true;
	}

}
