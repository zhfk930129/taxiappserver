package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import dataType.*;

public class Db {

	private static Connection con;
	
	public static void main(String[] args) {
		init(false);
		//la la la
	}

//=================================User===================================
	
	private static void testUser() {
		createPassenger("ziweiWang","heihei", " ", " ");
		createDriver("shuangchen","hehe", " ", " ");
		Float cnt = new Float(0.3);
		ArrayList<String> keyWords = new ArrayList<String>();
	}

	public static ArrayList<String> getAllPassengers() {
		try {
			Statement stmt = con.createStatement();
			stmt.execute("select name from passenger;");
			ResultSet rs = stmt.getResultSet();
			ArrayList<String> users = new ArrayList<String>();
			while(rs.next()) {
				users.add(rs.getString("name"));
			}
			return users;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 
	 * @param name
	 * @param pass
	 * @param type 1: Passenger; 2: Driver.
	 * @return true: success; false: failed.
	 */
	public static boolean createPassenger(String name, String nickname, String pass, String phonenumber) {
		try {
			PreparedStatement stmt  = con.prepareStatement(
					"insert into passenger(name, nickname, pass, phonenumber) values (?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, name);
			stmt.setString(2, nickname);
			stmt.setString(3, pass);
			stmt.setString(4, phonenumber);
			int ans = stmt.executeUpdate();
			System.out.println("DB result lines:" + ans);
			stmt.close();
			return ans > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	public static boolean deletePassenger(String name) {
		try {
			PreparedStatement stmt = con.prepareStatement(
					"delete from passenger where name=?");
			stmt.setString(1, name);
			int ans = stmt.executeUpdate();
			stmt.close();
			return ans > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean setPassengerPassword(String name, String password) {
		try {
			PreparedStatement stmt = con.prepareStatement(
					"update passenger set pass = ? where name = ?");
			stmt.setString(1, password);
			stmt.setString(2, name);
			int ans = stmt.executeUpdate();
			stmt.close();
			return ans > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean setPassengerNickname(String name, String nickName) {
		try {
			PreparedStatement stmt = con.prepareStatement(
					"update passenger set nickname = ? where name = ?");
			stmt.setString(1, nickName);
			stmt.setString(2, name);
			int ans = stmt.executeUpdate();
			stmt.close();
			return ans > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean setPassengerPhonenumber(String name, String phoneNumber) {
		try {
			PreparedStatement stmt = con.prepareStatement(
					"update passenger set phone = ? where name = ?");
			stmt.setString(1, phoneNumber);
			stmt.setString(2, name);
			int ans = stmt.executeUpdate();
			stmt.close();
			return ans > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static Passenger getPassengerByName(String name) {
		try {
			PreparedStatement stmt = con.prepareStatement(
					"select * from passenger where name = ?");
			stmt.setString(1, name);
			ResultSet result = stmt.executeQuery();
			String password = result.getString("pass"), nickName = result.getString("nickname"), phoneNumber = result.getString("phone");
			Passenger passenger = new Passenger(name, password, phoneNumber, nickName);
			return passenger;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 createDriver(name, password, phoneNumber, carNumber);
	 *
	 */
	public static boolean createDriver(String name, String pass, String phonenumber, String carnumber) {
		try {
			PreparedStatement stmt  = con.prepareStatement(
					"insert into passenger(name, pass, phonenumber, carnumber) values (?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, name);
			stmt.setString(2, pass);
			stmt.setString(3, phonenumber);
			stmt.setString(4, carnumber);
			int ans = stmt.executeUpdate();
			System.out.println("DB result lines:" + ans);
			stmt.close();
			return ans > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	public static boolean deleteDriver(String name) {
		try {
			PreparedStatement stmt = con.prepareStatement(
					"delete from driver where name=?");
			stmt.setString(1, name);
			int ans = stmt.executeUpdate();
			stmt.close();
			return ans > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean setDriverPassword(String name, String password) {
		try {
			PreparedStatement stmt = con.prepareStatement(
					"update driver set pass = ? where name = ?");
			stmt.setString(1, password);
			stmt.setString(2, name);
			int ans = stmt.executeUpdate();
			stmt.close();
			return ans > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean setDriverCarnumber(String name, String carNumber) {
		try {
			PreparedStatement stmt = con.prepareStatement(
					"update driver set carnumber = ? where name = ?");
			stmt.setString(1, carNumber);
			stmt.setString(2, name);
			int ans = stmt.executeUpdate();
			stmt.close();
			return ans > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean setDriverPhonenumber(String name, String phoneNumber) {
		try {
			PreparedStatement stmt = con.prepareStatement(
					"update driver set phone = ? where name = ?");
			stmt.setString(1, phoneNumber);
			stmt.setString(2, name);
			int ans = stmt.executeUpdate();
			stmt.close();
			return ans > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static Driver getDriverByName(String name) {
		try {
			PreparedStatement stmt = con.prepareStatement(
					"select * from driver where name = ?");
			stmt.setString(1, name);
			ResultSet result = stmt.executeQuery();
			String password = result.getString("pass"), carNumber = result.getString("carnumber"), phoneNumber = result.getString("phone");
			Driver driver = new Driver(name, password, phoneNumber, carNumber);
			return driver;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean setPassengerKeyWord(String name, ArrayList<String> keywords) {
		try {
			PreparedStatement stmt = con.prepareStatement(
					"update passenger set key_words=? where name = ?");
			stmt.setString(1, keywords.isEmpty()? "_":keywords.toString());
			stmt.setString(2, name);
			int ans = stmt.executeUpdate();
			stmt.close();
			return ans > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	

	/*
	 * @return	null if the user does not exist
	 * 			an empty ArrayList if the user exists but does not have keywords
	 * 			o.w. return a ArrayList containing the user's keywords
	 */	
	public static ArrayList<String> getPassengerKeyWords(String name) {
		try{
			PreparedStatement stmt = con.prepareStatement(
					"select key_words from passenger where name = ?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			ArrayList<String> keyWords = null;
			if (rs.next()){
				String key_words = rs.getString("key_words");
				keyWords = new ArrayList<String>();
				if (!key_words.equals("_")) {
					String[] words = key_words.substring(1, key_words.length()-1).split(",");
					for (int i = 0; i < words.length; ++i)
						keyWords.add(words[i]);
				}
			}
				
			stmt.close();
			return keyWords;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static ArrayList<String> getDriverKeyWords(String name) {
		try{
			PreparedStatement stmt = con.prepareStatement(
					"select key_words from driver where name = ?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			ArrayList<String> keyWords = null;
			if (rs.next()){
				String key_words = rs.getString("key_words");
				keyWords = new ArrayList<String>();
				if (!key_words.equals("_")) {
					String[] words = key_words.substring(1, key_words.length()-1).split(",");
					for (int i = 0; i < words.length; ++i)
						keyWords.add(words[i]);
				}
			}
				
			stmt.close();
			return keyWords;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/*
	 * @return	null if the user does not exist;
	 * 			return the password otherwise;
	 */
	public static String getPassengerPass(String name) {
		try {
			PreparedStatement stmt = con.prepareStatement(
					"select pass from passenger where name = ?");
			stmt.setString(1, name);
			
			ResultSet rs = stmt.executeQuery();
			String pass = null;
			if (rs.next()) {
				pass = rs.getString("pass");
				if (rs.wasNull())
					pass = null;
			}
			stmt.close();
			return pass;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	

	/*
	 * @return	null if the user does not exist;
	 * 			return the password otherwise;
	 */
	public static String getDriverPass(String name) {
		try {
			PreparedStatement stmt = con.prepareStatement(
					"select pass from driver where name = ?");
			stmt.setString(1, name);
			
			ResultSet rs = stmt.executeQuery();
			String pass = null;
			if (rs.next()) {
				pass = rs.getString("pass");
				if (rs.wasNull())
					pass = null;
			}
			stmt.close();
			return pass;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

//==========================Create tables===================================
	public static void init(boolean flag) {
		try {
			
			// Type 1: passenger; 2: Driver
			String create1 = "create table passenger ( " +
					"id int not null auto_increment, " +
					"name varchar(50) binary not null, " +
					"pass varchar(50) binary not null, " +
					"phone varchar(50) binary not null," +
					"nickname varchar(50) binary not null," +
					"key_words varchar(100) not null default '_', " +
					"primary key (id), " +
					"unique key (name) " +
					") engine = innodb, auto_increment = 1;";

			String create2 = "create table driver ( " +
					"id int not null auto_increment, " +
					"name varchar(50) binary not null, " +
					"pass varchar(50) binary not null, " +
					"phone varchar(50) binary not null," +
					"carnumber varchar(10) binary not null," +
					"key_words varchar(100) not null default '_', " +
					"primary key (id), " +
					"unique key (name) " +
					") engine = innodb, auto_increment = 1;";
			Class.forName("com.mysql.jdbc.Driver");
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String url="jdbc:mysql://localhost:3306/taxiApp";
			String user = "root";
			String password = "";
			con = DriverManager.getConnection(url, user, password);
			if (flag) {
				Statement stmt = con.createStatement();
				stmt.executeUpdate(create1);
				stmt.executeUpdate(create2);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
