package server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.lang.String;

import dataType.Driver;
import dataType.Passenger;
import db.Db;

/**
 * 该类为多线程类，用于服务端
 */
public class ServerThread implements Runnable {

	private Socket client = null;
	
	private static ArrayList<Passenger> loginPassengerList = new ArrayList<Passenger>();
	private static ArrayList<Passenger> requestPassengerList = new ArrayList<Passenger>();
	private static ArrayList<Driver> loginDriverList = new ArrayList<Driver>();
	private static ArrayList<Driver> workingDriverList = new ArrayList<Driver>();

	public ServerThread(Socket client) {
		this.client = client;
	}

	// 处理通信细节的静态方法，这里主要是方便线程池服务器的调用
	public static void execute(Socket client) {
		try {
			// 获取Socket的输出流，用来向客户端发送数据
			PrintStream out = new PrintStream(client.getOutputStream());
			// 获取Socket的输入流，用来接收从客户端发送过来的数据
			BufferedReader buf = new BufferedReader(new InputStreamReader(
					client.getInputStream()));
			boolean flag = true;
			while (flag) {
				// 接收从客户端发送过来的数据
				String strLine = buf.readLine();
				
				// REG_PASSENGER username nickname password
				if (strLine.startsWith("USER_REG_PASSENGER")) {
					String str = strLine.substring(strLine.indexOf(" ") + 1);
					String name = str.substring(0, str.indexOf(" "));
					str = str.substring(str.indexOf(" ") + 1);
					String nickname = str.substring(0, str.indexOf(" "));
					str = str.substring(str.indexOf(" ") + 1);
					String password = str.substring(0, str.indexOf(" " ));
					str = str.substring(str.indexOf(" ") + 1);
					String phonenumber = str;
					synchronized(ServerThread.class) {
						if (Db.createPassenger(name, nickname, password, phonenumber)) {
							out.println("ACK");
							out.flush();
						}
						else {
							out.println("NAK");
							out.flush();
						}
					}
					
				}
				// REG_DRIVER username password
				else if (strLine.startsWith("REG_DRIVER")) {
					String str = strLine.substring(strLine.indexOf(" ") + 1);
					String name = str.substring(0, str.indexOf(" "));
					str = str.substring(str.indexOf(" ") + 1);
					String password = str;
					
					//TODO  String process not done...
					if (Db.createDriver(name, password, "", "")) {
						out.println("ACK");
						out.flush();
					}
					else {
						out.println("NAK");
						out.flush();
					}
				}
				// USER_LOGIN username password
				else if (strLine.startsWith("USER_LOGIN")) {
					String str = strLine.substring(strLine.indexOf(" ") + 1);
					String username = str.substring(0, str.indexOf(" "));
					str = str.substring(str.indexOf(" "));
					String password = str;
					if (Db.getPassengerPass(username) != null) {
						if (Db.getPassengerPass(username).equals(password)) {
							Passenger passenger = Db.getPassengerByName(username);
							passenger.setIPAddress(client.getInetAddress().toString().split("/")[1]);
							synchronized(ServerThread.class) {
								loginPassengerList.remove(passenger);
								loginPassengerList.add(passenger);
							}
							out.println("ACK");
							out.flush();
						}
						else {
							out.println("NAK");
							out.flush();
						}
					}
					else {
						out.println("NAK");
						out.flush();
					}
				}
				// LOGOUT_PASSENGER username
				else if (strLine.startsWith("LOGOUT_PASSENGER")) {
					String username = strLine.substring(strLine.indexOf(" ") + 1);
					synchronized(ServerThread.class) {
						requestPassengerList.remove(Db.getPassengerByName(username));
						loginPassengerList.remove(Db.getPassengerByName(username));
					}
					out.println("ACK");
					out.flush();
				}
				else if (strLine.startsWith("USER_CALL")) {
					String str = strLine.substring(strLine.indexOf(" ") + 1);
					double stLng = Double.parseDouble(str.substring(0, str.indexOf(" ")));
					str = str.substring(str.indexOf(" ") + 1);
					double stLat = Double.parseDouble(str.substring(0, str.indexOf(" ")));
					str = str.substring(str.indexOf(" ") + 1);
					double enLng = Double.parseDouble(str.substring(0, str.indexOf(" ")));
					str = str.substring(str.indexOf(" ") + 1);
					double enLat = Double.parseDouble(str.substring(0, str.indexOf(" ")));
					str = str.substring(str.indexOf(" ") + 1);
					String username = str;
					Passenger passenger = Db.getPassengerByName(username);
					passenger.updateData(stLng, stLat, enLng, enLat);
					if (!loginPassengerList.contains(passenger)) {
						out.println("NAK You have to log in!");
						out.flush();
					}
					else {
						synchronized(ServerThread.class) {
							requestPassengerList.add(passenger);
						}
						out.println("ACK");
						out.flush();
					}
				}
				else if (strLine.startsWith("LOGIN_PASSENGER")) {
					
				}
				else if (strLine.startsWith("LOGIN_DRIVER")) {
					
				}
				else if (strLine.startsWith("LOGOUT_DRIVER")) {
					
				}
				else if (strLine.startsWith("CALLTAXI")) {
					
				}
				else if (strLine.startsWith("TAXIADDR")) {
					
				}
				else {
					out.println("NAK");
					out.flush();
					throw new Exception();
				}
			}
			out.close();
			buf.close();
			client.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void broadcastResult() {
		try {
			Process.excuteMatch(getRequestPassengerList(), getWorkingDriverList());
			for (Passenger p : requestPassengerList) {
				if (p.getMatch() != null) {
					Driver d = p.getMatch();
					Socket client = new Socket(p.getIPAddress(), 2334);
					PrintWriter os=new PrintWriter(client.getOutputStream());
					// MATCH:drivername car_num phone
					String str = "MATCH:" + d.getName() + " " + d.getCarNumber() + " " + d.getPhoneNumber();
					requestPassengerList.remove(p);
					os.println(str);
					os.flush();
					os.close();
					client.close();
					
					client = new Socket(d.getIPAddress(), 2334);
					os=new PrintWriter(client.getOutputStream());
					// MATCH:drivername car_num phone
					str = "MATCH:" + p.getNickName() + " " + d.getPhoneNumber();
					requestPassengerList.remove(d);
					os.println(str);
					os.flush();
					os.close();
					client.close();
				}
				else {
					Socket client = new Socket(p.getIPAddress(), 2334);
					PrintWriter os=new PrintWriter(client.getOutputStream());
					os.println("WAIT:");
					os.flush();
					os.close();
					client.close();
				}
			}
			for (Driver d : workingDriverList) {
				Socket client = new Socket(d.getIPAddress(), 2334);
				PrintWriter os=new PrintWriter(client.getOutputStream());
				os.println("WAIT:");
				os.flush();
				os.close();
				client.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
 	
	public static ArrayList<Passenger> getRequestPassengerList() {
		return requestPassengerList;
	}
	
	public static ArrayList<Driver> getWorkingDriverList() {
		return workingDriverList;
	}

	@Override
	public void run() {
		execute(client);
	}

}