package server;

import match.*;

import java.util.*;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;

import dataType.*;

public class Process {
	private static int[][] capacity;
	private static double[] fee;
	private static double[][] src_des;
	private static double[][] dis;
	private static int[][] matchRes;

	public static synchronized void excuteMatch(List<Passenger> passengerList,
			List<Driver> driverList) {
		clearResult();
		updateData(passengerList, driverList);
		matchRes = excuteMatch(passengerList.size(), driverList.size(),
				capacity, fee, src_des, dis);
		
		//TODO I don't really remember what matchRes means...
		for (int i = 0; i < passengerList.size(); ++i) {
			for (int j = 0; j < driverList.size(); ++j) {
				//TODO passengerList.get(i)
			}
		}
	}

	public static synchronized int[][] excuteMatch(int p, int t, int[][] c,
			double[] f, double[][] s, double[][] d) {
		int[][] ret = Match.match(p, t, c, f, s, d);
		return ret;
	}

	public static synchronized void updateData(List<Passenger> passengerList,
			List<Driver> driverList) {
		int m = passengerList.size(), n = driverList.size();
		fee = new double[m];
		for (int i = 0; i < m; ++i) {
			fee[i] = passengerList.get(i).getFee();
		}
		capacity = new int[m][n];
		for (int i = 0; i < m; ++i) {
			for (int j = 0; j < n; ++j) {
				capacity[i][j] = 1;
			}
		}
		try {
			updateSrcToDes(passengerList, m);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			updateDistance(passengerList, m, driverList, n);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static synchronized void clearResult() {
		capacity = null;
		fee = null;
		src_des = null;
		dis = null;
		matchRes = null;
	}

	public static synchronized void updateDistance(List<Passenger> passengerList, int m, List<Driver> driverList, int n) throws Exception {
		//TODO Every 5 be one group and send to baidu to accelerate query...
		dis = new double[m][n];
		for (int i = 0; i < m; ++i) {
			String origin = passengerList.get(i).getSlonToString() + "," + passengerList.get(i).getSlaToString();
			for (int j = 0; j < n; ++j) {	
				String destination = driverList.get(i).getLonToString() + "," + driverList.get(i).getLaToString();
				URL url = new URL(
						"http://api.map.baidu.com/direction/v1/routematrix?output=json&origins=" + origin + "&destinations=" + destination + "&ak=O1042qwcFPFFsk6TQlEtTR7N");
				Scanner scan = new Scanner(url.openStream());
				String str = new String();
				while (scan.hasNext())
			        str += scan.nextLine();
			    scan.close();
			    JSONObject obj = new JSONObject(str);
			    if (!obj.getString("message").equals("ok")) {
			    	System.out.println(obj.getInt("status"));
			    	throw new Exception();
			    }
			    JSONArray arr = obj.getJSONObject("result").getJSONArray("elements");
			    int distance = arr.getJSONObject(i).getJSONObject("distance").getInt("value");
			    System.out.println(distance);
			    dis[i][j] = distance;
			}
		}
	}

	public static synchronized void updateSrcToDes (
			List<Passenger> passengerList, int m) throws Exception {
		src_des = new double[m][m];
		//TODO Seems something wrong with it...
		for (int i = 0; i < m; ++i) {
			try {
				String origin = passengerList.get(i).getSlonToString() + "," + passengerList.get(i).getSlaToString();
				String destination = passengerList.get(i).getDlonToString() + "," + passengerList.get(i).getDlaToString();
				URL url = new URL(
						"http://api.map.baidu.com/direction/v1/routematrix?output=json&origins=" + origin + "&destinations=" + destination + "&ak=O1042qwcFPFFsk6TQlEtTR7N");
				Scanner scan = new Scanner(url.openStream());
				String str = new String();
				while (scan.hasNext())
			        str += scan.nextLine();
			    scan.close();
			    JSONObject obj = new JSONObject(str);
			    if (!obj.getString("message").equals("ok")) {
			    	System.out.println(obj.getInt("status"));
			    	throw new Exception();
			    }
			    JSONArray arr = obj.getJSONObject("result").getJSONArray("elements");
			    int distance = arr.getJSONObject(i).getJSONObject("distance").getInt("value");
			    System.out.println(distance);
			    //TODO Seems that there is something wrong...
			    //TODO src_des[i] = distance;
			    
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}
	}

}
