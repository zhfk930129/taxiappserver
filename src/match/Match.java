package match;

import java.util.*;

public class Match {

	public static int random() {
		Random r = new Random();
		return r.nextInt();
	}

	/**
	 * p-> people t-> taxi c-> able to match f-> fee s-> distance from the
	 * source point to the sink point d-> distance between the taxi and people
	 */
	public static int[][] match(int p, int t, int[][] c, double[] f,
			double[][] s, double[][] d) {

		int people = p;
		int taxi = t;
		/*
		 * //get people number people = 10; //get taxi number taxi = 20;
		 */
		// double

		int n = 2 + people + taxi;
		int[][] capM = new int[n + 1][n + 1];
		double[][] costM = new double[n + 1][n + 1];

		// get capM
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				capM[i][j] = 0;
			}
		}
		for (int j = 2; j <= people + 1; j++) {
			capM[1][j] = 1;
			capM[j][1] = Integer.MAX_VALUE;
		}
		for (int i = 2; i <= people + 1; i++) {
			for (int j = people + 2; j <= n - 1; j++) {
				if (c[i - 2][j - people - 2] == 1) {
					capM[i][j] = 1;
					capM[j][i] = Integer.MAX_VALUE;
				} else {
					capM[i][j] = Integer.MAX_VALUE;
					capM[j][i] = Integer.MAX_VALUE;
				}

			}
		}
		for (int i = people + 2; i <= n; i++) {
			capM[i][n] = 1;
			capM[n][i] = Integer.MAX_VALUE;
		}
		//
		// get costM
		// get distance from driver to passenger
		double[][] dis = new double[people][taxi];
		for (int i = 0; i < people; i++) {
			for (int j = 0; j < taxi; j++) {
				dis[i][j] = d[i][j];
			}
		}
		//
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				costM[i][j] = Integer.MAX_VALUE;
			}
		}
		for (int j = 2; j <= people + 1; j++) {
			costM[1][j] = 0;
		}
		for (int i = 2; i <= people + 1; i++) {
			for (int j = people + 2; j <= n - 1; j++) {
				costM[i][j] = (d[i - 2][j = people - 2])
						/ (s[i - 2][j - people - 2] + f[i - 2] / 2.5 + d[i - 2][j
								- people - 2]);
			}
		}
		for (int i = people + 2; i <= n - 1; i++) {
			costM[i][n] = 0;
		}
		//

		MGraph g = new MGraph(n, capM, costM);
		g.MinCostMaxFlow();

		int Mm = 0;
		int mc = 0;
		int[][] Mmr = new int[n + 1][n + 1];
		Mm = g.Mm;
		mc = g.mc;
		Mmr = g.Mmr;

		/*
		 * for(int i = 0; i <= n; i++){ for(int j = 0; j <= n; j++){
		 * System.out.println(Mmr[i][j]); } }
		 */
		int[][] out = new int[2][];
		out[0] = new int[people];
		out[1] = new int[taxi];

		for (int i = 2; i <= people + 1; i++) {
			for (int j = people + 2; j <= n - 1; j++) {
				if (Mmr[i][j] == 1) {
					out[0][i - 2] = j - people - 2;
					out[1][j - people - 2] = i - 2;
				} else {
					out[0][i - 2] = -1;
					out[1][j - people - 2] = -1;
				}
			}
		}
		return out;
		// analyze the flow
		// get the result of the matching

	}
}