package match;

import java.util.Vector;

public class MGraph {
	// ArrayList<ArrayList<Integer>> capacityMatrix = new
	// ArrayList<ArrayList<Integer>>();
	// ArrayList<ArrayList<Integer>> costMatrix = new
	// ArrayList<ArrayList<Integer>>();
	// int[][] matrix = Matrix(x,y);
	private int n;
	private int[][] capM;
	private double[][] costM;
	private int[][] path;
	private int[][] d;
	Vector<Integer> mcr = new Vector<Integer>();
	private double mrd = 0;
	int[][] Mmr;
	int Mm = 0;// finalflow
	int mc = 0;// mincost

	public MGraph(int num, int[][] a, double[][] b) {
		n = num;
		capM = new int[n + 1][n + 1];
		capM = a;
		costM = new double[n + 1][n + 1];
		costM = b;
		path = new int[n + 1][n + 1];
		Mmr = new int[n + 1][n + 1];
		// d = new int[n][n];
	}

	public void floyd() {
		// System.out.println("helloworld");

		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				path[i][j] = j;
			}
		}
		// System.out.println(path[1][n]+" 234");

		for (int k = 1; k <= n; k++) {
			for (int i = 1; i <= n; i++) {
				for (int j = 1; j <= n; j++) {
					if (costM[i][k] == Integer.MAX_VALUE
							|| costM[k][j] == Integer.MAX_VALUE) {

					}
					// if(costM[i][k]!=Integer.MAX_VALUE&&costM[k][j]!=Integer.MAX_VALUE)
					else if (costM[i][k] + costM[k][j] < costM[i][j]) {
						costM[i][j] = costM[i][k] + costM[k][j];
						path[i][j] = path[i][k];
					}
				}
			}
		}
		// System.out.println(path[1][n]+" 2345");
	}

	public void MinCostPath() {
		mcr.removeAllElements();
		floyd();
		int rd = 0;
		int tmp = 0;
		mrd = costM[1][n];
		rd = path[1][n];
		mcr.addElement(new Integer(0));
		mcr.addElement(new Integer(1));
		mcr.addElement(new Integer(rd));

		// System.out.println(path[rd][n]);
		// System.out.println(rd);
		while (rd != n) {
			tmp = path[rd][n];
			mcr.addElement(new Integer(tmp));
			rd = path[rd][n];
		}

	}

	public void MinCostMaxFlow() {
		// ArrayList<ArrayList<Integer>> A = capacityMatrix;
		// ArrayList<ArrayList<Integer>> C = costMatrix;
		/*
		 * int[][] A = capM; int[][] C = costM;
		 */
		int[][] A = new int[n + 1][n + 1];
		double[][] C = new double[n + 1][n + 1];

		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				A[i][j] = capM[i][j];
				C[i][j] = costM[i][j];
			}
		}

		int n1 = Integer.MAX_VALUE;// currentminflow
		MinCostPath();
		// System.out.println(mrd);

		while (mrd != Integer.MAX_VALUE) {
			// System.out.println("while");
			// System.out.println(mcr.size());
			for (int i = 1; i <= mcr.size() - 2; i++) {
				int ta = 0;

				if (capM[mcr.elementAt(i)][mcr.elementAt(i + 1)] == Integer.MAX_VALUE) {
					ta = A[mcr.elementAt(i + 1)][mcr.elementAt(i)]
							- capM[mcr.elementAt(i + 1)][mcr.elementAt(i)];
				} else {
					ta = capM[mcr.elementAt(i)][mcr.elementAt(i + 1)];
				}
				if (ta < n1)
					n1 = ta;
			}

			for (int i = 1; i <= mcr.size() - 2; i++) {
				// System.out.println("hello");
				if (capM[mcr.elementAt(i)][mcr.elementAt(i + 1)] == Integer.MAX_VALUE) {
					capM[mcr.elementAt(i + 1)][mcr.elementAt(i)] += n1;
					// System.out.println("+n");
				} else {
					capM[mcr.elementAt(i)][mcr.elementAt(i + 1)] -= n1;
					// System.out.println("-n");
				}
			}
			Mm += n1;
			for (int i = 1; i <= n; i++) {
				for (int j = 1; j <= n; j++) {
					if (i != j && capM[i][j] != Integer.MAX_VALUE) {
						if (capM[i][j] == A[i][j]) {
							costM[j][i] = Integer.MAX_VALUE;
							costM[i][j] = C[i][j];
						} else {
							if (capM[i][j] == 0) {
								costM[i][j] = Integer.MAX_VALUE;
								costM[j][i] = C[j][i];
							} else {
								costM[j][i] = C[j][i];
								costM[i][j] = C[i][j];
							}
						}
					}
				}
			}
			MinCostPath();
			n1 = Integer.MAX_VALUE;
		}

		/*
		 * for(int i = 1; i <= n; i++){ for(int j = 1; j <= n; j++){
		 * System.out.println(A[i][j]); } }
		 */

		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				if (A[i][j] == Integer.MAX_VALUE)
					A[i][j] = 0;
				if (capM[i][j] == Integer.MAX_VALUE)
					capM[i][j] = 0;
				Mmr[i][j] = A[i][j] - capM[i][j];
			}
		}

		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				if (Mmr[i][j] != 0)
					mc += Mmr[i][j] * C[i][j];
			}
		}
	}
}